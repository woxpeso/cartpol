# CARTPOL - Sistema Web para a Polícia Civil de Campina Grande

A polícia civil de Campina Grande, atualmente, engloba 70 delegacias localizadas na cidade e em seu entorno. Todo o registro de ocorrências é feito manualmente e/ou de maneira informatizada local, ou seja, não há integração de dados entre as delegacias. Hoje, não é possível, de forma ágil, obter relatórios sobre o número de ocorrências, suas localidades e quando foram registradas. Assim, o curso de Sistemas de Informação da Facisa, em cooperação com a Polícia Civil de Campina Grande, desenvolverá um sistema web misto (com componente offline) para o registro de ocorrências em um único banco de dados integrado, possibilitando a criação de relatórios gerenciais de grande valia para a administração pública e o planejamento de políticas de segurança para nossa cidade.

> Link: [http://cartpol.herokuapp.com](http://cartpol.herokuapp.com)

## Opções do projeto

### Deletando o cache.manifest

Para deletar o **cache.manifest**, acesse a URL:

	chrome://appcache-internals/