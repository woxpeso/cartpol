/*
 * Laboratório de Tecnologia da Informação - LTI - CESED - 2014
 * Todos os direitos reservados
 *
 * Software desenvolvido para a Polícia Civil de Campina Grande - PB
 *
 * Script com as funcoes que implementam o modulo controlador do login.
 */
 
cartpol.controlador.login = {
  exibirForm: function() {
    $("#barra-de-ferramentas").html("");
    toolkit.fillElementWithTemplate("main_content", "login/form", {}, cartpol.controlador.login.funcoesForm);
  },
  funcoesForm: function() {
    $("#login_form").submit(function(evt) {
      evt.preventDefault();
      // cartpol.controlador.principal.exibirForm();
      cartpol.controlador.informacaoGeral.exibirForm();
      return false;
    });
  }
};