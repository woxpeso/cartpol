var cartpol = {
  bancoDeDados: {
    usuarios: [
      {login: 'admin', senha: '21232f297a57a5a743894a0e4a801fc3'},
      {login: 'teste', senha: '698dc19d489c4e4db73e28a713eab07b'}
    ]
  },
  constantes: {
    DATABASE_NAME:  'cartpol',
    DATABASE_CLOUD: 'heroku.cloudant.com',
    DATABASE_OPTS:  {live: true},
    LANGUAGE:       'pt-BR'
  },
  configuraFormatos: function() {
    require(['numeral'], function() {
      numeral.language(cartpol.constantes.LANGUAGE, {
        delimiters: {
          thousands: '.',
          decimal: ','
        }
      });

      numeral.language(cartpol.constantes.LANGUAGE);
    });
  },
  dataBase: null,
  iniciaBancoDeDados: function() {
    require(['dataBase'], function(DataBase){
      cartpol.dataBase = new DataBase(cartpol.constantes.DATABASE_NAME);
      cartpol.dataBase.sincronizarBancoDeDados(cartpol.constantes.DATABASE_CLOUD, cartpol.constantes.DATABASE_OPTS);
      console.info('Banco de Dados Inicializado!');
    });
  },
  inicia: function() {
    cartpol.configuraFormatos();
    cartpol.iniciaBancoDeDados();
    require(['bootbox'], function(bootbox) {
      cartpol.bootbox = bootbox;
    });
  },
  // Módulo responsável por exibir janelas de diálogo
  bootbox: null,
  // Local para armazenar as variaveis globais do projeto
  globals: {
    informacaoGeral: null,
    bo: null
  },
  // Local onde os controladores serao definidos
  controlador: {}
};
